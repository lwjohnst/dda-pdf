---
output: 
    odt_document:
        reference_odt: resources/template.odt
---

<!-- describe scientific environment of project and signed confirmation from PI
and head of department that the project can be carried out where PI is employed
Need Letter head? I never remember what to add here at the top for letters...
-->

It is my pleasure to write this letter of recommendation for Dr. Luke Johnston
for his research project application for the Danish Diabetes Academy Postdoctoral
Fellowship. I first met Luke in 2015, during an international diabetes epidemiology
conference. From the outset, his engagement in discussions, grasp of both the
statistical and conceptual aspects of epidemiological analysis and enthusiasm
for the exploration of new ideas struck me as exceptional. The discussions we
had in the context of the conference clearly showed me that Luke has the
critical mindset needed to excel as a researcher. For over a year and a half,
Luke and I had frequent contact over Skype and email to discuss and develop a
post-doctoral project. In these exchanges Luke has shown clear independence,
perseverance, and strong organizational skills, setting himself goals and
following through on them. He set himself the goal of gaining knowledge and
experience with register-based epidemiological research in a Nordic country, 
which has now been realized since he recently joined our team on a research
grant of mine. His originality and ingenuity in problem solving have become very
clear from the outstanding and innovative project proposal he has written based
on an unfamiliar dataset, in a setting he has not worked in before
(e.g. register-based research in a Nordic country).

His proposal to study how early childhood conditions, based on multiple
"conditions" including geographic, environmental, and socioeconomic, influences
risk for type 2 diabetes and cardiometabolic risk using data from the entire
Danish population is at the cutting edge of register-based epidemiological
research in terms of scale and complexity. It will make use of linkages not only
between different data sources (different registers and cohort studies, as well
as geographic and environmental data), but also between different individuals
(e.g. to obtain parental socioeconomic status during childhood). Analysis of a
dataset of this scale and complexity requires the use of not just novel
statistical methods, but also computational and analytical methods. Our group
has been working increasingly with registry linkages and with these methods, 
Luke will find the environment perfect for honing his skills and also contributing
to our group's output.

Luke has clear and innovative ideas about how this adaptation and integration of
data and analysis may be accomplished and how it will succeed. His experience in
the development of packages for the R statistical computing language is also a
very strong asset to the success of the proposed project, and is a testament
to Luke's interest in pursuing new knowledge and discovery. Luke's talent and
proficiency in epidemiological research is clear from the excellent publications
he has authored, as well as from the ease with which he navigates basic and
advanced epidemiological concepts in discussions, presentations, and texts. 
His acheivements and talent have not gone unnoticed. In May 2017, he was
selected, and participated, as one of 12 highly promising young researchers to
attend a 1-week master class for prospective post-doctoral researchers at Aarhus
University. He has also received other awards during his PhD at the University
of Toronto, such as for student leadership and for research excellence, as well
as a highly competitive national 3 year doctoral award from the Canadian Diabetes
Association.

Luke is engaged in teaching data analysis in the R statistical package to
different audiences and he has contributed to the development of several R
packages. In fact, since he's arrived at Aarhus, he has set up a group composed
of like-minded Aarhus researchers to frequently teach about various analysis and
coding skills, and has already lead several training sessions. These activities,
along with the oral and poster presentations of his research results are clear
evidence for Luke’s ability and passion for communicating and disseminating his
skills, experience and research results. 

In my group we work with register-based projects and have close collaborations 
with the Steno Diabetes Center (e.g. Dr. Bendix Carstensen). We have recently
begun working on projects that link multiple datasets together. We will also be
moving to the new Steno Center in Aarhus, where we will be in an environment
dedicated to studying diabetes. Luke's project fits perfectly with my group's
overall research direction and, with the move to the new Steno Center, will be
afforded all the resources necessary to conduct his project.

I believe that Luke has strong leadership potential, as he possesses the
combination of solid subject matter knowledge, strong motivation, independence,
and strong analytical, practical and communication skills necessary to excel in
epidemiology in the age of large and complex data. I fully endorse and support
his application for the Danish Diabetes Academy Postdoctoral Fellowship Award
and would like to express my strongest support and recommendation.
    
Sincerely,


Daniel Witte
