---
fontsize: 11pt
papersize: a4
geometry: margin=2cm
output: 
    pdf_document: default
    odt_document:
        reference_odt: resources/template.odt
---

## Collaboration agreement

I am currently collaborating with Dr. Clemens Wittenbecher at the German
Institute for Human Nutrition, in Potsdam, Germany, on creating the NetCoupler
algorithm as an R software package for widespread scientific use. Our
collaboration details and discussions can be seen here:
https://github.com/ClemensWittenbecher/NetCoupler. Since the start of our 
collaboration, we have been adhering to open scientific principles by having all
method development and discussion occur in open, publicly accessible formats.
