---
fontsize: 12pt
papersize: a4
geometry: margin=2cm
output: pdf_document
---

# Publication list

**Number of first author publications**: 4

1. **Johnston LW**, Liu Z, Retnakaran R, Zinman B, Giacca A, Harris SB, Bazinet RP,
Hanley AJ. Clusters of fatty acids in the serum triacylglyceride fraction
associate with the disorders of type 2 diabetes. J Lipid Res (2016 JIF[^jif]: 4.81). 2018. Online ahead
of print. doi: 10.1194/jlr.P084970.
2. **Johnston LW**, Harris SB, Retnakaran R, Giacca A, Liu Z, Bazinet RP, Hanley AJ.
Association of non-esterified fatty acid composition with insulin sensitivity
and beta cell function in the Prospective Metabolism and Islet Cell Evaluation
(PROMISE) cohort. Diabetologia (2018 JIF: 6.023). 2017;61(821). doi: 10.1007/s00125-017-4534-6.
3. **Johnston LW**, Harris SB, Retnakaran R, Zinman B, Giacca A, Liu Z, Bazinet RP,
Hanley AJ. Longitudinal associations of phospholipid and cholesteryl ester fatty
acids with disorders underlying diabetes. J Clin Endocrinol Metab (2017/18 JIF: 5.789).
2016;101(6):2536-2544. doi: 10.1210/jc.2015-4267.
4. **Johnston LW**, Harris SB, Retnakaran R, Gerstein HC, Zinman B, Hamilton J,
Hanley AJ. Short leg length, a marker of early childhood deprivation, is
associated with metabolic disorders underlying type 2 diabetes mellitus: The
PROMISE cohort study. Diabetes Care (2017/18 JIF: 13.397). 2013;36(11):3599-3606. doi:
10.2337/dc13-0254.
5. Doughty K, Rothman L, **Johnston L**, Le K, Wu J, Howard A. Low-income
countries’ orthopaedic information needs: Challenges and opportunities. Clinical
Orthopaedics and Related Research (2017/18 JIF: 4.091). 2010;468:2598-2603. doi:
10.1007/s11999-010-1365-x.

[^jif]: While this is a requirement for the DDA Fellowship application, I
strongly oppose the use of the JIF for assessment, for reasons as laid out in
the DORA Declaration (https://sfdora.org/read/).
