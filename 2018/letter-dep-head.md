---
output: 
    odt_document:
        reference_odt: resources/template.odt
---

<!-- 

describe *scientific environment* of project and signed confirmation from PI
and head of department that the project can be carried out where PI is employed

Focus more on how I will fit in and what support and infrastructure will be 
available/provided.
-->

Dear review committee,

It is with pleasure that I write this letter of support for Dr. Luke Johnston on
his application to the Postdoctoral Fellowship from the Danish Diabetes Academy.
Judging from Luke's CV, the proposed project, and his supervisor's (Dr. Daniel
Witte) scientific track record, this project will be innovative and of high
importance and impact. It is excellently suited for the newly setup Steno
Diabetes Center in Aarhus.

Luke will study how early life adversity affects the risk of type 2 diabetes in
adult life based on an innovative integration of multiple Danish and
international cohorts and registers. The Steno Diabetes Center (SCD) is a world
leader in diabetes research and patient care. Our mission is to continue
delivering first rate care to patients and to improve our knowledge and
understanding through scientific inquiries on and innovations in the prevention
and treatment of diabetes. Luke's project fits precisely into our mission and
goal in producing high-quality and impactful research and to attracting bright
and talented researchers. While Dr. Witte is currently at Aarhus University, he
and his research group will be moving over to our new institute later in the
year.

The SCD Aarhus offers an excellent environment for this project, as it is a
recognized research entity with Statistics Denmark, allowing us access to the
research servers, which is a crucial aspect to Luke's project. In our new
research facilities, Luke will have everything he needs (office space,
computers, a multidisciplinary collegiate environment) to efficiently and
productively conduct his research. He will be integrated into our research
environment, where he will be exposed to a cross-disciplinary, highly
knowledgeable, and passionate team of researchers.

Luke is a perfect fit to the SCD Aarhus, which is well-suited to host Luke's
project. Luke brings with him his international network (he completed his PhD at
the University of Toronto), as well as the proposed project plan to collaborate
internationally and work with researchers from the German Institute for Human
Nutrition on developing statistical techniques needed for his project. We would
greatly welcome Luke to become part of our institute.

Kind Regards,


Niels Jessen
