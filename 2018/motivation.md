---
fontsize: 11pt
papersize: a4
geometry: margin=2cm
output: 
    pdf_document: default
    odt_document:
        reference_odt: resources/template.odt
---

<!-- max 1 page -->

### Motivation letter

My **primary goal** for moving to Denmark for research and for this fellowship
project is to learn about and conduct register-based studies with linkages to
cohorts. I created and planned this proposal as it will achieve many of my other
career and scientific goals. Given this, there are several reasons why I am
precisely fit to complete this project. Firstly, I am familiar with the study of
early life origins on later disease. My interest in this field began during my
undergraduate studies and is the reason my Master's research was studying the
role of adult leg length (a biomarker of early childhood conditions) on diabetes
pathophysiology. Secondly, I have become very competent at working with -omic
style data and at using advanced analytical techniques through my PhD research,
which was on a broad spectrum of fatty acids in serum and on diabetes. Thirdly,
I've authored multiple R packages and incorporated many software development
practices in my research workflow. I incorporated these practices for several
reasons: so I can learn to program; to adhere to open scientific and
reproducible principles; and to create reusable tools that my (past and present)
colleagues can use to simplify their own projects. Fourthly, I am an experienced
and highly competent researcher, as evidenced by my publications in respected
diabetes journals (Diabetes Care, Diabetologia).

Myself and the project support the **the three strategic areas**. I bring
*international collaborations* through: a) my network with a diabetes
epidemiology group in the University of Toronto (Dr. Anthony Hanley); b) my
collaborative project and 3-month research stay with Dr. Wittenbecher at DIfE in
Potsdam; c) being a Canadian researcher working in Denmark; d) my R coding
instruction and open science networks e.g. Software and Data Carpentry and the
UofTCoders[^uoftcoders] in Toronto. The project has multiple *interdisciplinary
collaborations*, including: mathematical theory, statistical learning, software
development, and open scientific practices for NetCoupler and my other open
source R packages; and, epidemiology, economics, and geography for the early
life conditions and diabetes. *Cross sector collaborations* will occur through
the joint university and hospital setting, at the newly established Steno
Diabetes Center Aarhus (SDCA). I also do a considerable amount of instructing
and collaborating with people in the open source and open science community,
e.g. Mozilla Science Lab[^msl]. These communities have vast networks of
researchers working on open science projects in a wide range of fields and
sectors, including private, non-governmental, and non-profit. I also contribute
to open source software packages (e.g. broom[^broom]) that are maintained by
private sector companies (e.g. RStudio).

[^broom]: https://github.com/tidymodels/broom
[^uoftcoders]: UofTCoders (uoftcoders.github.io) I helped found. We are
collaborating on a "coding training for scientists" project.
[^msl]: I have completed many projects with them e.g. the Open Project Leader
training.

There are many exceptional opportunities with this fellowship to advance my
**future career** as an epidemiologist and research scientist and for acquiring
new skills and knowledge, which are incredibly valuable for conducting modern
epidemiological research. My **main career goals** for this project are to
become competent in register-based studies, to hone my R programming and
statistical analysis skills and knowledge, to develop a stronger network with
European diabetes epidemiologists, and to initiate and work on collaborative
projects, especially where there is a focus on methods development (e.g. with
Dr. Wittenbecher). My **scientific knowledge goals** are to become much more
familiar with the field of early life origins of disease, to become
knowledgeable of the growing -omics field, and to learn more about causal
reasoning in epidemiology. More generally, I hope to hone my interpersonal and
public-speaking communication, teamwork, and leadership skills. Through this
fellowship, I will achieve all of these goals by conducting the research and
analyses, collaborating with Dr. Wittenbecher, doing the 3 month research stay
in Potsdam, and working in the team environment in Dr. Witte's group as well as
in helping set up the epidemiology research group at SDCA. These are all a means
to build the foundation for my future research plans. Combining my current
skills, knowledge, and network of collaborators, and by completing this
fellowship, I will be well positioned to becoming a *more successful and
stronger independent scientist*.

I **currently work** at Aarhus University as a *postdoctoral researcher* with
Dr. Witte since February, 2018. We are in the process of moving to the SDCA.
This is an exciting new move, as we will be working in close proximity with
other diabetes researchers in the new facilities and in a hospital setting.
