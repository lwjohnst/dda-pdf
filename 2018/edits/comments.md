

- In the available space, perhaps you can include a flow chart with the time planning
for the different objectives and the Potsdam visit. 
- Perhaps re-introduce one or two examples of analyses that you will carry out
to approach the main objective. The proposal now gives a clear impression of
what you want to achieve with regard to the package development; this same level
of clarity of what will be achieved needs to be there for the main objective
too. 

