---
fontsize: 12pt
papersize: a4
geometry: margin=2cm
output: 
    pdf_document: default
    odt_document:
        reference_odt: resources/template.odt
---

**3. Motivation letter**

My **primary goal** for moving to Denmark for research was to learn about and
conduct register-based studies with linkages to cohorts. I developed this
fellowship proposal to achieve my career and scientific goals. There are several
reasons why *I am precisely fit* to complete this project. First, I am familiar
with the early life origins on later disease field, where my Master's research
was on the role of adult leg length (a biomarker of early childhood conditions)
on type 2 diabetes (T2D). Second, I am very competent in -omic style data
analysis and in advanced analytics from my PhD, which was on analyzing
a spectrum of serum fatty acids and T2D. Third, I've authored R packages and
incorporated software development practices into my research workflow[^github],
which allow me to tackle challenging research questions from a reproducible and
open scientific framework. Fourth, I am an experienced and highly competent
researcher, as evidenced by my publications in respected diabetes journals
(Diabetes Care, Diabetologia).

[^github]: See https://github.com/lwjohnst86 for examples of my open source and
analytic work.

Myself and this project support the **three strategic areas**. I bring
*international collaborations* through: a) my connection to a diabetes
epidemiology group at the University of Toronto (Dr. Anthony Hanley); b) my
collaborative project and planned research stays with Dr. Wittenbecher in
Potsdam, Germany; c) being a Canadian researcher working in Denmark; d) my R
coding instruction and open science networks e.g. Software and Data Carpentry.
The project has multiple *interdisciplinary collaborations*, including:
statistical learning, software development, and open scientific practices for
NetCoupler and other R packages; and, epidemiology and demography for the early
life conditions and T2D. *Cross sector collaborations* will occur through the
joint university and hospital setting at the recently established Steno Diabetes
Center Aarhus (SDCA). I also do a considerable amount of instructing and
collaborating with people in the open science community, e.g. Mozilla Science
Lab, who have a vast network of researchers working in a wide range of fields and
sectors, e.g. private or non-profit.

My **primary career goal** is to be a diabetes epidemiologist with expertise in
data science and complex big data analyses.
For this project, my aims are: 1) to become competent in register-based studies
and cohort linkages; 2) to develop stronger networks with European diabetes
epidemiologists; 3) to increase my expertise in the early life origins of
disease field and in the -omics field. Through the project's objectives, by
taking workshops at BERTHA, and through Dr Witte's supervision, I will learn
about and become a specialist at working with "big data", in both depth
(register) and width (-omics). While completing O2 (pathway analysis) and
O3 (algorithmic methods development), collaborating with Dr Wittenbecher on
NetCoupler (O3), and being involved in Dr Witte's group's research, I will
become adept at causal pathway and clustering techniques. *Data science skills*
are increasingly being recognized as crucial to remaining relevant and
competitive in scientific research, which I will acquire throughout this
fellowship. For the next stage of my career I want to focus more on studying diabetes
complications (such as cardiovascular disease and neuropathy), to expand my scientific
knowledge base. The scale and
scope of the project's results and the knowledge I will gain, combined with the
toolkits and workflows that I create, will open many new avenues of research for
me to further investigate.
My next intended step after this fellowship will be to apply for an EU ERC
Starting Grant to begin developing my own research programme, which will situate
me to achieving my career goal and continuing on to being an *independent*
diabetes epidemiologist.

I **currently work** at Aarhus University as a *postdoctoral researcher* with
Dr. Witte since February, 2018. With this fellowship I will move to the new
SDCA, giving me great opportunity to gain experience in a mixed
hospital-research setting and to move closer to being independent.
