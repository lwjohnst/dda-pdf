
Just a short comment on your motivation letter. With the DDA in mind, perhaps
frame your plans for the future as becoming an independent diabetes
epidemiologist with expertise and focus on complex big data analysis, rather
than saying you’d like to expand to other chronic diseases. If you want to
include other chronic diseases, perhaps frame them as diabetes complications
(CVD, neuropathy, DKD, etc).

With regard to the collaboration documents, do you think you could get Clemens
to draft and sign a full letter on Potsdam letterhead, confirming your
collaboration and welcoming you to visit Potsdam as part of this project? I
think that would make a more solid impression than just a statement written by
you.
