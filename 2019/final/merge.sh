#!/bin/sh

# R command used:
# fs::dir_ls("2019/final/", glob = "*.pdf") %>% str_remove("2019/final/") %>% cat(sep = " \\\n")

pdfunite \
    02-research-plan.pdf \
    03-motivation.pdf \
    04-cv-applicant.pdf \
    05-publication-list.pdf \
    06a-masters-diploma.pdf \
    06b-graduate-grades.pdf \
    07-phd-diploma.pdf \
    08-letter-support-steno.pdf \
    08-letter-support-supervisor.pdf \
    09-cv-daniel-witte.pdf \
    10-collaboration-agreement-dife.pdf \
    full-application.pdf
