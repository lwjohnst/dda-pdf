---
fontsize: 10pt
papersize: a4
geometry: margin=2cm
output: 
    pdf_document:
        includes:
            in_header: resources/header.tex
---

## 4. CV: Luke W. Johnston

ORCID: 0000-0003-4169-2616  
Full CV: http://cv.lukewjohnston.com

-----

**Education**

Sep, 2013 -- Nov, 2017
: *PhD*, University of Toronto, Canada. [Serum Composition of Fatty Acids in Lipid Fractions in the Progression of Metabolic Abnormalities Underlying Type 2 Diabetes Mellitus.](http://hdl.handle.net/1807/80893) Supervisor: Dr. Anthony J. Hanley. Co-supervisor: Dr. Richard Bazinet

Sep, 2011 -- Nov, 2013
: *MSc*, University of Toronto, Canada. [Association of Leg Length with Metabolic Abnormalities Underlying Type 2 Diabetes Mellitus.](http://hdl.handle.net/1807/42975) Supervisor: Dr. Anthony J. Hanley. 

Sep, 2005 -- Jun, 2010
: *Honours BSc in Kinesiology*, University of Waterloo, Canada

**Teaching experience and curriculum development**

Mar, 2019
: Workshop material: *Reproducible Quantitative Methods: Data analysis workflow using R*, Danish Diabetes Academy, Denmark.

Jun, 2018 --- present
: *Instructor and founder*, Peer-led, participatory live coding R training sessions, Aarhus University Open Coders. Website: https://au-oc.github.io.

Jul, 2018
: Workshop material: *FAIR Data and Software*, TIB Leibniz Information Centre for Science and Technology and University Library, Hannover, Germany. Website: https://tibhannover.github.io/2018-07-09-FAIR-Data-and-Software/. 

Jun, 2015 -- Jan, 2018
: *Instructor*, Programming code-alongs, UofT Coders, University of Toronto, Canada. Website: http://uoftcoders.github.io.

Sep, 2017 -- Dec, 2017
: Course material: *Theoretical Ecology and Reproducible Quantitative Methods in R (EEB430) course material*, Department of Ecology and Evolutionary Biology, University of Toronto, Canada. Website: https://github.com/uoftcoders/rcourse. DOI: [10.5281/zenodo.1117432](https://doi.org/10.5281/zenodo.1117432).

2015 -- 2017
: *Instructor*, multiple workshops for [Software Carpentry](http://software-carpentry.org).

**Oral presentations**

1. **Johnston LW**. An R toolkit to simplify and automate an open scientific workflow. In: *European R User Meeting*. Budapest, Hungary; 2018. 
 2. **Johnston LW**, Lee C, Harris S, Retnakaran R, Zinman B, Bazinet RP, Hanley AJ. Serum Phospholipid (PL) Fatty Acid Composition Predicts Declines in Insulin Sensitivity (IS) and Beta-Cell Function Over 6-years in the Prospective Metabolism And Islet Cell Evaluation (PROMISE) Cohort. In: *International Diabetes Epidemiology Group Scientific Meeting*. Vancouver, Canada; 2015. <https://figshare.com/s/1cce7abacad54e48aa0a>. 
 3. **Johnston L**, Lee C, Harris S, Retnakaran R, Zinman B, Bazinet R, Hanley A. Serum Non-Esterified Fatty Acid (NEFA) Concentrations are Associated with Longitudinal Progression of Beta-Cell Dysfunction: Prospective Metabolism and Islet Cell Evaluation (PROMISE) Cohort. In: *33rd International Symposium on Diabetes & Nutrition in Toronto, Canada*. Toronto, Canada; 2015. doi: [10.6084/m9.figshare.1545567](https://doi.org/10.6084/m9.figshare.1545567). 
 4. **Johnston L**, Kayaniyil S, Lee C, Harris S, Retnakaran R, Zinman B, Bazinet R, Hanley A. Lower Serum Non-Esterified Eicosapentaenoic Acid (EPA) is Associated with Insulin Resistance in the PROspective Metabolism and ISlet Cell Evaluation (PROMISE) Cohort. In: *Biennial Congress of the International Society for the Study of Fatty Acids and Lipids*. Stockholm, Sweden; 2014. <https://figshare.com/s/89dbdcb4695f19cbd2b1>. 

**Oral poster presentations**

1. **Johnston LW**, Zhen L, Retnakaran R, Harris SB, Zinman B, Bazinet RP, Hanley AJ. Leg length, a marker of early childhood conditions, associates with specific clusters of serum fatty acids. In: *Annual Meeting of the European Association for the Study of Diabetes*. Berlin, Germany; 2018. 
 2. **Johnston LW**, Liu Z, Retnakaran R, Harris SB, Bazinet RP, Hanley AJ. Specific clusters of fatty acids (FA) across multiple serum lipid fractions underlie pathophysiological features of type 2 diabetes in the Canadian Prospective Metabolism and Islet Cell Evaluation (PROMISE) cohort. In: *European Diabetes Epidemiology Group Annual Meeting*. Helsingør, Denmark; 2018. doi: [10.6084/m9.figshare.6159293](https://doi.org/10.6084/m9.figshare.6159293). 
 3. **Johnston LW**, Lee C, Harris SB, Retnakaran R, Zinman B, Bazinet RP, Hanley AJ. Serum Non-Esterified Fatty Acid (NEFA) Concentration is Associated with Longitudinal Progression of Beta-Cell Dysfunction: Prospective Metabolism and Islet Cell Evaluation (PROMISE) Cohort. *Diabetes*. 2015;64(Suppl. 1):A421, 1621-P. doi: [10.6084/m9.figshare.1545566](https://doi.org/10.6084/m9.figshare.1545566).

**Conference posters**

1. Santaren ID, Bazinet RP, Liu Z, **Johnston LW**, Retnakaran R, Harris SB, Zinman B, Hanley AJ. Combining dietary and biomarker data to evaluate associations of dairy intake with risk of type 2 diabetes (T2DM): The PROspective Metabolism and ISlet cell Evaluation (PROMISE) cohort. In: *European Diabetes Epidemiology Group Annual Meeting*. Helsingør, Denmark; 2018. 
 2. Semnani-Azad Z, Connelly PW, **Johnston LW**, Retnakaran R, Harris SB, Zinman B, Hanley AJ. Longitudinal Associations of Soluble CD163 with Insulin Sensitivity and Beta-Cell Function: The Prospective Metabolism and Islet Cell Evaluation (PROMISE) Cohort. In: *4th Annual BBDC-Joslin-UCPH Conference*. Toronto, Canada; 2017. 
 3. **Johnston LW**, Retnakaran R, Harris SB, Liu Z, Bazinet RP, Hanley. AJ. Fatty Acids Produced by De Novo Lipogenesis (DNL) of Refined Carbohydrates are Associated with Worsening of Metabolic Syndrome (MetS) Components: The Prospective Metabolism and Islet Cell Evaluation (PROMISE) Cohort. *Diabetes*. 2017;66(Supplement 1)(1669-P):A445. doi: [10.2337/db17-1489-1795](https://doi.org/10.2337/db17-1489-1795). 
 4. Wang WZ, Cole DE, **Johnston LW**, Wong BY, Fu L, Retnakaran R, Harris SB, Zinman B, Hanley. AJ. Urinary Vitamin D Binding Protein as a Potential Biomarker for Nephropathy in Subjects at Risk for Type 2 Diabetes: The Prospective Metabolism and Islet Cell Evaluation (PROMISE) Cohort Study. *Diabetes*. 2017;66(Supplement 1)(1627-P):A435. doi: [10.2337/db17-1489-1795](https://doi.org/10.2337/db17-1489-1795). 
 5. Semnani-Azad Z, Connelly PW, **Johnston LW**, Retnakaran R, Harris SB, Zinman B, Hanley AJ. Longitudinal Associations of Soluble CD163 with Insulin Sensitivity and Beta-Cell Function: The Prospective Metabolism and Islet Cell Evaluation (PROMISE) Cohort. *Diabetes*. 2017;66(Supplement 1)(1674-P):A447. doi: [10.2337/db17-1489-1795](https://doi.org/10.2337/db17-1489-1795). 
 6. **Johnston LW**, Retnakaran R, Harris S, Zinman B, Liu Z, Bazinet RP, Hanley AJ. Triglyceride Fatty Acid (TGFA) Composition Longitudinally Associates with Changes in Insulin Sensitivity (IS) and Beta-Cell Function Over 6-yrs in the Prospective Metabolism and Islet Cell Evaluation (PROMISE) Cohort. *Diabetes*. 2016;65(Supplemental 1):A406. doi: [10.6084/m9.figshare.3422767](https://doi.org/10.6084/m9.figshare.3422767). 
 7. Semnani-Azad Z, **Johnston LW**, Harris SB, Retnakaran R, Zinman B, Connelly PW, Hanley. AJ. Multivariate Determinants of Reduced Insulin Clearance in Prospective Metabolism and Islet Cell Evaluation (PROMISE) Cohort. *Diabetes*. 2016;65(Supplement 1):A405. doi: [10.2337/db16-1375-1656](https://doi.org/10.2337/db16-1375-1656). 
 8. **Johnston LW**, Lee C, Harris SB, Retnakaran R, Zinman B, Bazinet RP, Hanley AJ. Serum Non-Esterified Fatty Acid (NEFA) Composition and Longitudinal Associations with Beta-Cell Dysfunction. In: *International Diabetes Federation World Diabetes Congress*. Vancouver, Canada; 2015. <https://figshare.com/s/7301482497f6a6349dc8>. 
 9. **Johnston L**, Kayaniyil S, Lee C, Harris S, Retnakaran R, Zinman B, Bazinet R, Hanley A. Lower Serum Non-Esterified Eicosapentaenoic Acid (EPA) Is Associated with Insulin Resistance: PROspective Metabolism and ISlet Cell Evaluation (PROMISE) Cohort. *Diabetes*. 2014;63(Suppl. 1):A384, 1463-P. <https://figshare.com/s/a7cf86f77bc4be78ac7f>. 
 10. **Johnston LW**, Harris SB, Retnakaran R, Gerstein HC, Hamilton J, Zinman B, Hanley AJ. The association of leg length with metabolic abnormalities underlying type 2 diabetes mellitus: The PROMISE cohort. *Diabetes*. 2013;62(Suppl. 1):A912, 1578-P. <https://figshare.com/s/271bfd6c988efcdb5921>. 

**Published CRAN R packages**

[prodigenr](https://cran.r-project.org/package=prodigenr) 
:  _Research Project Directory Generator_:  Create a project directory structure, along with typical files for that project.  This allows projects to be quickly and easily created, as well as for them to be standardized. *First published*:  Apr, 2016 

[mason](https://cran.r-project.org/package=mason) 
:  _Build Data Structures for Common Statistical Analysis_:  Use a consistent syntax to create data structures of common statistical techniques that can be continued in a magrittr pipe chain. Design the analysis, add settings and variables, construct the results, and polish the final structure. *First published*:  Jul, 2016 

[carpenter](https://cran.r-project.org/package=carpenter) 
:  _Build Common Tables of Summary Statistics for Reports_:  Easily create tables commonly used in scientific papers that can be reproduced and automatically updated whenever new data is included or from suggestions by reviewers. *First published*:  Jul, 2016 

**Awards**

Nov, 2017
: *Michael C. Archer Research Excellence Award*, University of Toronto, Canada. Recognition for the best research article published in 2016 by a graduate student in the Department of Nutritional Sciences.

Sep, 2014 -- Aug, 2017
: *Doctoral Student Research Award*, Canadian Diabetes Association. Nationally competitive award.

Apr, 2017
: *Gordon Cressy Student Leadership Award*, University of Toronto, Canada. For recognition of outstanding extra-curricular contributions to the faculty and the university as a whole.

Sep, 2014 -- Aug, 2015
: *Graduate Novo Nordisk Studentship*, Banting and Best Diabetes Centre, University of Toronto, Canada.

Jun, 2014
: *New Investigator Award*, International Society for the Study of Fatty Acids and Lipids.

