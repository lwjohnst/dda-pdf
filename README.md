
# Application instructions and notes

The postdoctoral fellowships should be within diabetes and metabolism.
Applications supporting the three major strategic areas of the DDA
(internationalization, interdisciplinarity and collaboration across sectors)
will be prioritized.

With these postdoctoral fellowships, the DDA wants to support researchers who
are in the beginning of their research careers and develop/consolidate their
individual research profile and scientific network. The DDA emphasises mobility.

A change of research/work environment (i.e. study abroad or working in another
sector, i.e. general practice, industry, university hospital) will be
prioritized.

You must upload your application in one single PDF file only! The application
should include the following documents, which should be numbered as specified
below:

0. Title page: Name of applicant and project title

1. Abstract of 1/2 A4 page (in English)

2. Research plan including:

    - Specific aims
    - Background and significance of the project to the research area
    - Preliminary studies (if applicable)
    - Research design and methods
    - Literature cited 

    Page limit for the research plan is 5 pages (font size 12, single spacing),
    including figures and tables, but excluding references - applications with
    research plans exceeding the page limit will not be considered.

3. Motivation letter (max 1 page) including:

    - An explanation of why you are the right candidate to complete the project
    - Description of how the project and you support the three major strategic
    areas of DDA***
    - Future career plans
    - If you plan to continue your postdoctoral studies in the same
    department/institution as you conducted your PhD studies, please include a
    description of the advantages of continuing your studies in the same
    research environment
    - Information about current position, place of work and name of principal
    investigator

4. Your Curriculum Vitae (including teaching experience, oral and poster
presentations at important relevant conferences, assessment and review
activities, received grants and/or awards, mobility and international
experience) (max 2 pages)

5. Publication list with impact factor and number of first author publications

6. Copy of your Master’s degree certificate (including grades)

7. Copy of your PhD diploma or statement from your principal supervisor*

8. Recommendation letter and acceptance from principal investigator and head of
department:

    - Description of the scientific environment where the postdoctoral project
    will be carried out, including signed confirmation from principal supervisor
    and head of department that the project can be carried out at the
    institution where the principal investigator is employed

9. Curriculum Vitae of principal investigator (max 2 pages)

10. Collaboration agreement with other research groups nationally or
internationally

11. If applying for a part-time postdoctoral position, documentation of clinical
employment from head of department must be attached

12. Other relevant documentation (e.g. recommendations, IELTS certificate
(international standardized certificate of English proficiency))
